Name:           python-sure
Version:        2.0.1
Release:        1
Summary:        Utility belt for automated testing in Python
License:        GPLv3+
URL:            https://github.com/gabrielfalcao/sure
Source0:        %{pypi_source sure}
# https://github.com/gabrielfalcao/sure/commit/9f0e834
# https://src.fedoraproject.org/rpms/python-sure/pull-request/1
Patch0:         python3.10-workaround.patch

BuildRequires:  python3-devel python3-mock python3-setuptools python3-six python3-pytest python-pytest-cov
BuildArch:      noarch

%description
An idiomatic testing library for python with powerful and flexible assertions. Sure is heavily
inspired in RSpec Expectations and should.js.

%package -n python3-sure
Summary:        Utility belt for automated testing in Python 3
Requires:       python3-six
%{?python_provide:%python_provide python3-sure}

%description -n python3-sure
An idiomatic testing library for python with powerful and flexible assertions. Sure is heavily
inspired in RSpec Expectations and should.js.

%prep
%autosetup -n sure-%{version} -p1
cp -r sure/ tests/
rm -rf sure.egg-info

%build
%py3_build

%install
%py3_install

%check
/usr/bin/pytest --ignore=tests/test_old_api.py

%files -n python3-sure
%doc COPYING
%{_bindir}/sure
%{python3_sitelib}/*

%changelog
* Wed Jun 14 2023 Ge Wang <wang__ge@126.com> - 2.0.1-1
- Upgrade to version 2.0.1

* Fri Apr 29 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 2.0.0-1
- upgrad 2.0.0

* Fri Apr 01 2022 wangkai <wangkai385@huawei.com> - 1.4.11-7
- Fix compilation failures

* Wed Aug 05 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 1.4.11-6
- Remove python2

* Thu May 21 2020 yanan li <liyanan032@huawei.com> - 1.4.11-5
- Modify python-mock and python-nose version

* Wed Nov 27 2019 lingsheng <lingsheng@huawei.com> 1.4.11-4
* Package init
